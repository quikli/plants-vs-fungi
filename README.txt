This entry uses the python pysdl2 and pyrsistent libraires (as shown in requirements.txt).

To install the python dependencies, run:

    pip install -r requirements.txt

Then install SDL2 runtime binaries in the manner appropriate for your operating system.
pysdl2 has dependencies on SDL2 runtime binaries, installation of which
varies from platform to platform. See installation instructions below if you need help.

To run the game:

    python run_game.py


## Windows SDL2 Installation ##

Those binaries have been included in the windows 64-bit
distribution of this game, and will need to be placed somewhere on your path to be
found by pysdl2's ctypes-based loader.

Placing them in your virtual envrionment's scripts folder should work and not
require any special system permissions.  You can also download them from libsdl.org manually.  You'll need SDL2, SDL2_image, SDL2_ttf, and SDL2_mixer (each 

Copy them from: <this-folder>\sdl_binaries\*.dll
Place them in: <virtual-environment-path>\Scripts\*.dll


## Linux SDL2 Installation ##

Installation method of SDL2 on linux will vary depending on your distribution.
Instructions for Ubuntu/Debian and Fedora are below, but other distributions
may need to adjust these based on the package manager or package repositories available.

Ubuntu:

    sudo apt install libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libsdl2-mixer-dev

Fedora:

    sudo yum install SDL2 SDL2_image SDL2_mixer SDL2_ttf


## Mac SDL2 Installation ##

I don't own any apple products and don't know how to set up SDL2 on a mac.
I found an article that just says to do:

    brew install sdl2

I'm not sure if this will also install the SDL2 extra libraries (mixer, ttf, and image),
or if those need additional installation steps.

