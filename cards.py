import random

from pyrsistent import freeze, thaw, PClass, field, pvector, ny, pmap
from vector import V2

from renderables import Image, Text, Animation
from utils import clamp

NEXT_CARD_ID = 1

FONT = "to-the-point-regular.ttf"

CARDS = {
    "devils-bolete": {
        "type": "creation",
        "faction": "fungi",
        "description": "Non-Attacking fungi that generates armor to protect itself",
        "anchor": V2(0.5, 0.6),
    },
    "mycena": {
        "type": "creation",
        "faction": "fungi",
        "description": "Small mushrooms that will damage all plants a small amount",
        "anchor": V2(0.5, 0.75),
    },
    "panther-cap": {
        "type": "creation",
        "faction": "fungi",
        "description": "A highly poisonous mushroom that will damage a random plant each turn",
        "anchor": V2(0.5, 0.55),
    },
    "tree": {
        "type": "creation",
        "faction": "plant",
        "description": "Non-Attacking plant that generates armor to protect itself",
        "anchor": V2(0.5, 0.9),
    },
    "venus-flytrap": {
        "type": "creation",
        "faction": "plant",
        "description": "A carnivorous plant that will damage a random fungus each turn",
        "anchor": V2(0.4, 0.8),
    },
    "vines": {
        "type": "creation",
        "faction": "plant",
        "description": "A plant that causes a random fungus i to not take their turn action",
        "anchor": V2(0.15, 0.65),
    },
    "blight": {
        "type": "damage",
        "faction": "fungi",
        "description": "Causes a plant to start to taking 20% damage a turn for 5 turns",
    },
    "slug": {
        "type": "damage",
        "faction": "plant",
        "description": "Consumes fungi, causing 50% damage to a random fungus",
    },
    "amantia": {
        "type": "utility",
        "faction": "fungi",
        "description": "Generates armor for a random fungus",
    },
    "humidity": {
        "type": "utility",
        "faction": "fungi",
        "description": "Causes a lingering drizzle that fills humidity, but refills some ground water.",
    },
    "night": {
        "type": "utility",
        "faction": "fungi",
        "description": "Changes the current time to dusk",
    },
    "slime-mold": {
        "type": "utility",
        "faction": "fungi",
        "description": "Produces energy (+2 actions)",
    },
    "spores": {
        "type": "utility",
        "faction": "fungi",
        "description": "Draw 2 cards from your deck",
    },
    "bees": {
        "type": "utility",
        "faction": "plant",
        "description": "Helps a plant breed, duplicating it",
    },
    "bush": {
        "type": "utility",
        "faction": "plant",
        "description": "Generates armor for a random plant",
    },
    "day": {
        "type": "utility",
        "faction": "plant",
        "description": "Changes the current time to dawn",
    },
    "fertilizer": {
        "type": "utility",
        "faction": "plant",
        "description": "Increases the growth rate of a plant",
    },
    "rain": {
        "type": "utility",
        "faction": "plant",
        "description": "Refills the ground water, but causes some humidity",
    },
}

CARDS = freeze(CARDS)


class Card(PClass):
    name = ""
    is_creation = False

    number = field(type=int, mandatory=True)
    growth = field(type=float, mandatory=True, initial=1.0)
    previous_growth = field(type=float, mandatory=True, initial=0.0)
    armor = field(type=float, mandatory=True, initial=0.0)

    @property
    def anchor(self):
        if self.is_creation:
            return CARDS[self.name].anchor
        else:
            return V2(0.5, 0.5)

    @staticmethod
    def spawn(name):
        global NEXT_CARD_ID
        creation = CARD_FACTORY[name](number=NEXT_CARD_ID)
        NEXT_CARD_ID += 1
        return creation

    @property
    def kind(self):
        return CARDS[self.name]['type']

    @property
    def faction(self):
        return CARDS[self.name]['faction']

    @property
    def description(self):
        return CARDS[self.name]['description']

    def render(
        self,
        start_scale=1.0,
        end_scale=1.0,
        start_position=V2(0, 0),
        end_position=V2(0, 0),
        on_hover=None,
        on_click=None,
        duration=0.25,
        name=None,
    ):
        yield Image(
            name=self.name,
            is_composite=True,
            on_hover=on_hover,
            on_click=on_click,
            animations=pmap({
                'scale': Animation(
                    name=name,
                    start_value=start_scale,
                    end_value=end_scale,
                    duration=0.25,
                ),
                'position': Animation(
                    name=name,
                    start_value=start_position,
                    end_value=end_position,
                    duration=0.25,
                ),
            }),
        )
        yield Text(
            name=FONT,
            size=64,
            text=self.name.title(),
            animations=pmap({
                'scale': Animation(
                    start_value=start_scale,
                    end_value=end_scale,
                    duration=0.25,
                ),
                'position': Animation(
                    start_value=start_position + V2(0, -270) * start_scale,
                    end_value=end_position + V2(0, -270) * end_scale,
                    duration=0.25,
                ),
            }),
        )
        yield Text(
            name=FONT,
            size=42,
            text=self.description,
            wrap=350,
            animations=pmap({
                'scale': Animation(
                    start_value=start_scale,
                    end_value=end_scale,
                    duration=0.25,
                ),
                'position': Animation(
                    start_value=start_position + V2(0, 75) * start_scale,
                    end_value=end_position + V2(0, 75) * end_scale,
                    duration=0.25,
                ),
            }),
        )

    def apply_act(self, state, player):
        return self.act(state, player)

    def act(self, state, player):
        return state

    def play(self, state, player):
        pvalue = getattr(state, player)
        assert pvalue.energy > 0
        if self.is_creation:
            return state.set(player, pvalue.set(
                deck=pvalue.deck.remove_from_hand(self),
                creations=pvalue.creations.set(self.number, self),
                energy=pvalue.energy - 1,
            ))
        pvalue = pvalue.set(deck=pvalue.deck.move_to_grave(self), energy=pvalue.energy - 1)
        state = state.set(player, pvalue)
        return self.apply_act(state, player)


class Creation(Card):
    is_creation = True
    growth_rate = field(type=float, mandatory=True, initial=1.20)
    dot = field(type=tuple, mandatory=True, initial=(0.0, 0))
    restrained = field(type=bool, mandatory=True, initial=False)

    def take_damage(self, damage):
        damage_after_armor = damage - self.armor
        if damage_after_armor < 0:
            return self.set(armor=self.armor - damage)
        else:
            return self.set(armor=0.0, growth=self.growth - damage_after_armor)

    def grow(self, growth_rate):
        return self.set(
            growth=self.growth * growth_rate * self.growth_rate,
            previous_growth=self.growth,
        ).regen(growth_rate).decay()

    def decay(self):
        if self.dot[1] > 0:
            return self.set(
                dot=(self.dot[0], self.dot[1] - 1),
                growth=self.growth * self.dot[0],
            )
        else:
            return self

    def regen(self, growth_rate):
        return self

    def apply_act(self, state, player):
        if self.restrained:
            pvalue = getattr(state, player)
            return state.set(
                player,
                pvalue.set(
                    creations=pvalue.creations.set(
                        self.number,
                        self.set(restrained=False)
                    )
                )
            )
        else:
            return self.act(state, player)



class Tank(Creation):
    armor = field(type=float, mandatory=True, initial=1.0)

    def regen(self, growth_rate):
        return self.set(armor=self.armor * growth_rate + 0.1)


class Tree(Tank):
    name = "tree"


class DevilsBolete(Tank):
    name = "devils-bolete"


class Mycena(Creation):
    name = "mycena"

    def act(self, state, player):
        return apply_to_all_enemies(lambda c: c.take_damage(self.growth * 0.1), state, player)


class PantherCap(Creation):
    name = "panther-cap"

    def act(self, state, player):
        return apply_to_random_enemy(lambda c: c.take_damage(self.growth * 0.4), state, player)


class VenusFlytrap(Creation):
    name = "venus-flytrap"

    def act(self, state, player):
        return apply_to_random_enemy(lambda c: c.take_damage(self.growth * 0.4), state, player)


class Vines(Creation):
    name = "vines"

    def act(self, state, player):
        return apply_to_random_enemy(lambda c: c.set(restrained=True), state, player)


class Blight(Card):
    name = "blight"

    def act(self, state, player):
        return apply_to_random_enemy(lambda c: c.set(dot=(0.8, 5)), state, player)


class Slug(Card):
    name = "slug"

    def act(self, state, player):
        return apply_to_random_enemy(lambda c: c.set(growth=c.growth * 0.5), state, player)


class Amantia(Card):
    name = "amantia"

    def act(self, state, player):
        return apply_to_random_ally(lambda c: c.set(armor=c.armor + 0.5), state, player)


class Humidity(Card):
    name = "humidity"

    def act(self, state, player):
        return state.set(humidity=1.0, water=clamp(state.water + 0.25, 0.0, 1.0))


class Night(Card):
    name = "night"

    def act(self, state, player):
        return state.set(time=0.50)


class SlimeMold(Card):
    name = "slime-mold"

    def act(self, state, player):
        return apply_to_player(lambda p: p.set(energy=p.energy + 2), state, player)


class Spores(Card):
    name = "spores"

    def act(self, state, player):
        return apply_to_player(lambda p: p.set(deck=p.deck.draw(2)), state, player)
        player_value = getattr(state, player)
        return state.set(player, player_value.set(deck=player_value.deck.draw(2)))


class Bees(Card):
    name = "bees"

    def act(self, state, player):
        card = get_random_card(state, player)
        if card:
            new_card = card.set(number=Card.spawn(card.name).number)
            return state.transform(
                [player, 'creations'],
                lambda c: c.set(new_card.number, new_card),
            )
        else:
            return state


class Bush(Card):
    name = "bush"

    def act(self, state, player):
        return apply_to_random_ally(lambda c: c.set(armor=c.armor + 0.5), state, player)


class Day(Card):
    name = "day"

    def act(self, state, player):
        return state.set(time=0.0)


class Fertilizer(Card):
    name = "fertilizer"

    def act(self, state, player):
        return apply_to_random_ally(lambda c: c.set(growth_rate=c.growth_rate + 0.1), state, player)


class Rain(Card):
    name = "rain"

    def act(self, state, player):
        return state.set(water=1.0, humidity=clamp(state.humidity + 0.25, 0.0, 1.0))


CARD_FACTORY = {
    "tree": Tree,
    "devils-bolete": DevilsBolete,
    "mycena": Mycena,
    "panther-cap": PantherCap,
    "venus-flytrap": VenusFlytrap,
    "vines": Vines,
    "blight": Blight,
    "slug": Slug,
    "amantia": Amantia,
    "humidity": Humidity,
    "night": Night,
    "slime-mold": SlimeMold,
    "spores": Spores,
    "bees": Bees,
    "bush": Bush,
    "day": Day,
    "fertilizer": Fertilizer,
    "rain": Rain,
}


def other_player(player):
    if player == 'player':
        return 'enemy'
    elif player == 'enemy':
        return 'player'


def get_random_card(state, player):
    cards = getattr(state, player).creations
    if len(cards) > 0:
        return random.choice(cards.values())
    else:
        return None


def apply_to_random_ally(fn, state, player):
    card = get_random_card(state, player)
    if not card:
        return state
    return state.transform([player, 'creations', card.number], fn)


def apply_to_all_allies(fn, state, player):
    return state.transform([player, 'creations', ny], fn)


def apply_to_random_enemy(fn, state, player):
    return apply_to_random_ally(fn, state, other_player(player))


def apply_to_all_enemies(fn, state, player):
    return apply_to_all_allies(fn, state, other_player(player))


def apply_to_player(fn, state, player):
    return state.set(player, fn(getattr(state, player)))


def spawn_random(faction):
    return Card.spawn(random.choice(list(c for c in CARDS if CARDS[c]['faction'] == faction)))

