import time


class Clock:
    def __init__(self, frame_time):
        self.frame_time = frame_time
        self.times = {}
        self.start_frame()

    def start_frame(self):
        self.start_time = time.monotonic()

    def time_since(self, key):
        now = time.monotonic()
        last_value = self.times.get(key, self.start_time)
        self.times[key] = now
        return now - last_value

    def end_frame(self):
        now = time.monotonic()
        target = self.start_time + self.frame_time
        if now < target:
            time.sleep(target - now)
        self.start_frame()
