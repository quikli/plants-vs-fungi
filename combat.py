import enum
import math
import random

from pyrsistent import PClass, field, pvector, pvector_field, pmap_field, pset, pdeque, PDeque, pmap

import cards
import stats
import menu

from cards import Card
from deck import Deck
from renderables import Image, Text, FilledRect, Animation
from utils import clamp, interpolate, composition
from vector import V2


FONT = "to-the-point-regular.ttf"

class LevelReward(enum.Enum):
    INCREASED_GROWTH = 'Increased Growth Rate (+10%)'
    INCREASED_HAND_SIZE = 'Increased Hand Size (+1 Card)'
    INCREASED_ENERGY = 'Increased Energy per Turn (+1 Energy)'


class Phase(enum.Enum):
    PLAYERDRAW = 'playerdraw'
    PLAYER = 'player'
    ENEMYDRAW = 'enemydraw'
    ENEMY = 'enemy'
    ALLIES = 'allies'
    ENEMIES = 'enemies'
    GROWTH = 'growth'
    TIME = 'time'
    VICTORY = 'victory'
    DEFEAT = 'defeat'


class Player(PClass):
    deck = field(type=Deck, mandatory=True)
    faction = field(type=str, mandatory=True)
    energy = field(type=int, mandatory=True, initial=0)
    max_energy = field(type=int, mandatory=True, initial=3)
    hand_size = field(type=int, mandatory=True)
    growth_rate = field(type=float, mandatory=True)
    creations = pmap_field(int, Card)

    @property
    def growth(self):
        return sum(c.growth for c in self.creations.values())


class State(PClass):
    player = field(type=Player, mandatory=True)
    enemy = field(type=Player, mandatory=True)
    humidity = field(type=float, mandatory=True, initial=0.5)
    water = field(type=float, mandatory=True, initial=0.5)
    time = field(type=float, mandatory=True, initial=0.75)
    phase = field(type=Phase, mandatory=True, initial=Phase.PLAYERDRAW)
    focus = field(mandatory=True, initial=None)
    card_queue = field(type=PDeque, mandatory=True, initial=pdeque())
    card_rewards = pvector_field(Card)
    level_rewards = pvector_field(LevelReward)
    enemy_level = field(type=int, mandatory=True)
    animation = field(mandatory=True, initial=None)
    animation_context = field(mandatory=True, initial=None)
    animation_result = field(mandatory=True, initial=None)

    @property
    def plant_growth_rate(self):
        time = self.time - 1if self.time > 0.75 else self.time
        time_factor = 0.50 - abs(time - 0.25)
        water_factor = self.water / 2
        base_rate = 1.10
        value = base_rate ** (time_factor * water_factor)
        return value

    @property
    def fungi_growth_rate(self):
        time = self.time + 1 if self.time < 0.25 else self.time
        time_factor = 0.10 - abs(time - 0.75)
        humidity_factor = self.humidity / 2
        base_rate = 1.10
        value = base_rate ** (time_factor * humidity_factor)
        return value

    def growth_rate(self, side):
        if side == 'plant':
            return self.plant_growth_rate
        elif side == 'fungi':
            return self.fungi_growth_rate

    def advance_time(self):
        new_time = self.time + 0.10
        if new_time > 1.0:
            new_time -= 1.0
        humidity = self.humidity
        for _ in self.enemy.creations.values():
            humidity = humidity * 0.9
        water = self.water
        for _ in self.player.creations.values():
            water = water * 0.9
        return self.set(time=new_time, water=water, humidity=humidity)

    def clear_the_dead(self):
        new_player_creations = pmap({n: c for n, c in self.player.creations.items() if c.growth > 0.25})
        new_enemy_creations = pmap({n: c for n, c in self.enemy.creations.items() if c.growth > 0.25})
        return self.set(
            player=self.player.set(creations=new_player_creations),
            enemy=self.enemy.set(creations=new_enemy_creations),
        )

    @property
    def growth_diff(self):
        player_growth = self.player.growth
        enemy_growth = self.enemy.growth
        total_growth = player_growth + enemy_growth
        return clamp(player_growth - enemy_growth, -15, 15)

    def check_victory(self):
        level = stats.get_level()
        if self.growth_diff > 10:
            stats.add_experience(300)
            experience = stats.get_experience()
            if experience >= stats.experience_for(level + 1):
                level_rewards = pvector(LevelReward)
            else:
                level_rewards = pvector([])
            return self.set(
                phase=Phase.VICTORY,
                card_rewards=pvector(cards.spawn_random(self.player.faction) for _ in range(3)),
                level_rewards=level_rewards,
            )
        elif self.growth_diff < -10:
            stats.add_experience(100)
            experience = stats.get_experience()
            if experience >= stats.experience_for(level + 1):
                level_rewards = pvector(LevelReward)
            else:
                level_rewards = pvector([])
            return self.set(
                phase=Phase.DEFEAT,
                card_rewards=pvector(cards.spawn_random(self.player.faction) for _ in range(1)),
                level_rewards=level_rewards,
            )
        else:
            return self


def update(mixer, event, state):
    event_name, event_value = event

    if event_name == 'focus':
        if isinstance(event_value, Card) and event_value != state.focus:
            if event_value in state.player.deck.hand:
                mixer.play_sfx('playcard', 1.0)
            elif event_value in state.card_rewards:
                mixer.play_sfx('playcard', 1.0)
        elif event_value in state.level_rewards:
            mixer.play_sfx('click', 1.0)
        return state.set(focus=event_value)

    elif event_name == 'finish turn':
        mixer.play_sfx('playcard', 1.0)
        return state.set(phase=Phase.ENEMYDRAW)

    elif event_name == 'use card':
        mixer.play_sfx('playcard', 1.0)
        return event_value.play(state, 'player').clear_the_dead()

    elif event_name == 'add to collection':
        mixer.play_sfx('click', 1.0)
        deck = state.player.deck.add_to_collection(event_value)
        deck.save(f'player-{state.player.faction}-deck.json')
        return state.set(player=state.player.set(deck=deck), card_rewards=pvector([]), focus=None)

    elif event_name == 'skip card reward':
        mixer.play_sfx('click', 1.0)
        return state.set(card_rewards=pvector([]), focus=None)

    elif event_name == 'select level reward':
        mixer.play_sfx('click', 1.0)
        stats.add_reward(event_value.value)
        return state.set(level_rewards=pvector([]), focus=None)

    elif event_name == 'leave':
        if event_value == 'combat':
            return make_new(mixer, state.enemy_level + 1)
        else:
            return menu.State()

    elif event_name == 'elapsed':

        if state.phase == Phase.PLAYERDRAW:
            if len(state.player.deck.hand) >= state.player.hand_size:
                return state.set(phase=Phase.PLAYER, player=state.player.set(energy=state.player.max_energy))
            elif len(state.player.deck.deck) == 0:
                return state.set(phase=Phase.PLAYER, player=state.player.set(energy=state.player.max_energy))
            else:
                mixer.play_sfx('deal1', 1.0)
                return state.set(player=state.player.set(deck=state.player.deck.draw(1)))

        elif state.phase == Phase.PLAYER:
            if state.player.energy == 0:
                return state.set(phase=Phase.ENEMYDRAW)
            else:
                return state

        elif state.phase == Phase.ENEMYDRAW:
            if len(state.enemy.deck.hand) >= state.enemy.hand_size:
                return state.set(phase=Phase.ENEMY, enemy=state.enemy.set(energy=state.enemy.max_energy))
            elif len(state.enemy.deck.deck) == 0:
                return state.set(phase=Phase.ENEMY, enemy=state.enemy.set(energy=state.enemy.max_energy))
            else:
                return state.set(enemy=state.enemy.set(deck=state.enemy.deck.draw(1)))

        elif state.phase == Phase.ENEMY:
            if state.enemy.energy > 0 and len(state.enemy.deck.hand) > 0:
                if state.animation:
                    if state.animation < event_value.current_time:
                        return state.animation_result
                    else:
                        return state
                else:
                    mixer.play_sfx('playcard', 1.0)
                    return state.set(
                        animation=event_value.current_time + 2.0,
                        animation_context=state.enemy.deck.hand[0],
                        animation_result=state.enemy.deck.hand[0].play(state, 'enemy').clear_the_dead(),
                    )
            else:
                return state.set(phase=Phase.ALLIES, card_queue=pdeque(state.player.creations.values()))

        elif state.phase == Phase.ALLIES:
            if len(state.card_queue) == 0:
                return state.set(phase=Phase.ENEMIES, card_queue=pdeque(state.enemy.creations.values()))
            else:
                return state.card_queue.right.apply_act(state, 'player').set(
                    card_queue=state.card_queue.pop()
                ).clear_the_dead()

        elif state.phase == Phase.ENEMIES:
            if len(state.card_queue) == 0:
                mixer.play_sfx('growth', 1.0)
                return state.set(
                    phase=Phase.GROWTH,
                    player=state.player.set(energy=state.player.energy + 1),
                    enemy=state.enemy.set(energy=state.enemy.energy + 1),
                )
            else:
                return state.card_queue.right.act(state, 'enemy').set(
                    card_queue=state.card_queue.pop()
                ).clear_the_dead()

        elif state.phase == Phase.GROWTH:
            if state.player.energy > 0:
                state = cards.apply_to_all_allies(
                    lambda c: c.grow(state.growth_rate(state.player.faction) * state.player.growth_rate),
                    state,
                    'player'
                )
                return state.set(
                    player=state.player.set(
                        energy=state.player.energy - 1
                    )
                )
            elif state.enemy.energy > 0:
                state = cards.apply_to_all_allies(
                    lambda c: c.grow(state.growth_rate(state.enemy.faction) * state.enemy.growth_rate),
                    state,
                    'enemy'
                )
                return state.set(
                    enemy=state.enemy.set(
                        energy=state.enemy.energy - 1
                    )
                )
            if state.animation:
                if state.animation < event_value.current_time:
                    return state.animation_result
                else:
                    return state
            else:
                return state.set(
                    animation=event_value.current_time + 2.0,
                    animation_context=None,
                    animation_result=state.set(phase=Phase.TIME),
                )

        elif state.phase == Phase.TIME:
            new_state = state.set(phase=Phase.PLAYERDRAW).advance_time().check_victory()
            if new_state.phase in [Phase.VICTORY, Phase.DEFEAT]:
                mixer.play_music('results', 0.15, fade_duration=2.0)
            return new_state


        elif state.phase == Phase.VICTORY:
            return state

        elif state.phase == Phase.DEFEAT:
            return state

        else:
            return state

    else:
        return state


def render(state):
    yield from render_background(state)
    yield from render_player_creations(state)
    yield from render_enemy_creations(state)
    if state.phase in [Phase.PLAYERDRAW, Phase.PLAYER]:
        yield from render_hand(state)
    if state.phase == Phase.ENEMY:
        yield from render_enemy_play(state)
    yield from render_ui(state)
    if state.phase == Phase.PLAYER:
        yield from render_turn_button(state)
        yield from render_overlay(state)
    if state.phase in [Phase.VICTORY, Phase.DEFEAT]:
        yield from render_reward(state)


def render_reward(state):
    if state.phase == Phase.VICTORY:
        yield Text(name=FONT, size=200, text="VICTORY!", position=V2(960, 75))
    elif state.phase == Phase.DEFEAT:
        yield Text(name=FONT, size=200, text="DEFEAT!", position=V2(960, 75))

    if len(state.card_rewards) > 0:
        yield Text(name=FONT, size=128, text="Choose a card to add to your collection", position=V2(960, 200))
        if len(state.card_rewards) == 1:
            card = state.card_rewards[0]
            yield from card.render(
                name=f'card-{card.number}',
                start_scale=0.8 if state.focus and state.focus == card else 1.0,
                end_scale=1.0 if state.focus and state.focus == card else 0.8,
                start_position=V2(960, 600),
                end_position=V2(960, 600),
                on_hover=('focus', card),
                on_click=('add to collection', card),
            )
        else:
            for i, card in enumerate(state.card_rewards):
                yield from card.render(
                    name=f'card-{card.number}',
                    start_scale=0.8 if state.focus and state.focus == card else 1.0,
                    end_scale=1.0 if state.focus and state.focus == card else 0.8,
                    start_position=V2((i + 1) * 480, 600),
                    end_position=V2((i + 1) * 480, 600),
                    on_hover=('focus', card),
                    on_click=('add to collection', card),
                )
        yield Text(
            name=FONT,
            text="SKIP",
            size=128,
            position=V2(960, 950),
            on_hover=('focus', 'skip'),
            on_click=('skip card reward', None),
            animations=pmap({
                'scale': Animation(
                    start_value=1.0 if state.focus and state.focus == 'skip' else 1.2,
                    end_value=1.2 if state.focus and state.focus == 'skip' else 1.0,
                    duration=0.25,
                ),
            })
        )

    elif len(state.level_rewards) > 0:
        yield Text(name=FONT, size=128, text="Level up! Choose a reward.", position=V2(960, 175))
        for i, reward in enumerate(LevelReward):
            yield Text(
                name=FONT,
                size=128,
                text=reward.value,
                position=V2(960, 400 + i * 150),
                on_hover=('focus', reward),
                on_click=('select level reward', reward),
                animations=pmap({
                    'scale': Animation(
                        name=reward,
                        start_value=1.0 if state.focus and state.focus == reward else 1.2,
                        end_value=1.2 if state.focus and state.focus == reward else 1.0,
                        duration=0.25,
                    )
                })
            )

    elif state.phase == Phase.VICTORY:
        yield Text(name=FONT, size=256, text="Continue?", position=V2(960, 500))
        size = 256 if state.focus and state.focus == 'yes' else 164
        yield Text(
            name=FONT,
            size=size,
            text="YES",
            position=V2(640, 700),
            on_hover=('focus', 'yes'),
            on_click=('leave', 'combat'),
        )
        size = 256 if state.focus and state.focus == 'no' else 164
        yield Text(
            name=FONT,
            size=size,
            text="NO",
            position=V2(1280, 700),
            on_hover=('focus', 'no'),
            on_click=('leave', 'menu'),
        )
    else:
        yield Text(name=FONT, size=128, text=f"Maximum level reached: {state.enemy_level}", position=V2(960, 250))
        size = 128 if state.focus and state.focus == 'leave' else 96
        yield Text(
            name=FONT,
            size=size,
            text="Return to Menu",
            position=V2(960, 575),
            on_hover=('focus', 'leave'),
            on_click=('leave', 'menu'),
        )


def render_background(state):
    yield Image(name="combat-background", anchor=V2(0, 0), on_hover=('focus', None))


def render_hand(state):
    for i, card in enumerate(state.player.deck.hand):
        yield from card.render(
            start_scale=0.6,
            end_scale=0.6,
            start_position=V2(200 + i * 180, 1080),
            end_position=V2(200 + i * 180, 1080),
            on_hover=('focus', card),
        )


def render_player_creations(state):
    end_point = V2(100, 800)
    start_point = V2(900, 550)
    number_of_items = len(state.player.creations.values())
    for i, (card_number, card) in enumerate(sorted(state.player.creations.items())):
        position = interpolate(start_point, end_point, (i + 1) / (number_of_items + 1))
        yield Image(
            name=card.name,
            position=position,
            anchor=card.anchor,
            on_hover=('focus', card),
            animations=pmap({
                'scale': Animation(
                    name=f'creation-{card.number}',
                    start_value=math.log(1 + card.previous_growth * 0.25),
                    end_value=math.log(1 + card.growth * 0.25),
                )
            })
        )


def render_enemy_creations(state):
    end_point = V2(1920 - 100, 800)
    start_point = V2(1920 - 900, 550)
    number_of_items = len(state.enemy.creations.values())
    for i, (card_number, card) in enumerate(sorted(state.enemy.creations.items())):
        position = interpolate(start_point, end_point, (i + 1) / (number_of_items + 1))
        yield Image(
            name=card.name,
            position=position,
            anchor=card.anchor,
            on_hover=('focus', card),
            animations=pmap({
                'scale': Animation(
                    name=f'creation-{card.number}',
                    start_value=math.log(1 + card.previous_growth * 0.25),
                    end_value=math.log(1 + card.growth * 0.25),
                )
            })
        )


def render_enemy_play(state):
    if state.animation:
        yield from state.animation_context.render(
            name=f'enemy-card-{state.animation_context.number}',
            start_scale=0.6,
            end_scale=1.0,
            start_position=V2(1920 / 2, 2000),
            end_position=V2(1920/ 2, 540),
        )


def render_ui(state):
    yield Image(
        name="stat-gauges",
        position=V2(10, 10),
        anchor=V2(0, 0),
        on_hover=('focus', 'stat-gauges'),

    )
    yield Image(
        name='stat-marker',
        position=interpolate(V2(75, 35), V2(295, 35), state.time),
    )
    yield Image(
        name='stat-marker',
        position=interpolate(V2(75, 85), V2(295, 85), state.water),
    )
    yield Image(
        name='stat-marker',
        position=interpolate(V2(75, 135), V2(295, 135), state.humidity),
    )
    actual_value = interpolate(305, 875, composition(-10, 10, state.growth_diff))
    player_start_position = 390
    player_size = actual_value
    enemy_start_position = player_start_position + actual_value
    enemy_size = 1545 - enemy_start_position
    yield FilledRect(
        position=V2(player_start_position, 30),
        size=V2(player_size, 90),
        color=(0, 255, 125, 255),
        anchor=V2(0, 0),
    )
    yield FilledRect(
        position=V2(enemy_start_position, 30),
        size=V2(enemy_size, 90),
        color=(255, 125, 0, 255),
        anchor=V2(0, 0),
    )
    yield Image(
        name="growth-bar",
        position=V2(1920 / 2, 75),
        scale=0.8,
        on_hover=('focus', 'growth-bar'),
    )

    if state.focus and state.focus == 'stat-gauges':
        yield Image(
            name="smoke-background",
            anchor=V2(1, 0),
            position=V2(1920, 100),
        )
        lines = [
            'Time, Water, and Humidity',
            'Plants want sunlight and Water',
            'Fungi want darkness and humidity',
        ]
        for j, line in enumerate(lines):
            yield Text(
                name=FONT,
                size=64,
                text=line,
                position=V2(1350, 250 + 50 * j),
                anchor=V2(0, 0),
            )

    if state.focus and state.focus == 'growth-bar':
        yield Image(
            name="smoke-background",
            anchor=V2(1, 0),
            position=V2(1920, 100),
        )
        lines = [
            f'Player Growth: {state.player.growth:.03f}',
            f'Opponent Growth: {state.enemy.growth:.03f}',
            'Game ends when player or enemy',
            'is winning by 10 or more growth',
            'at the end of both turns',
        ]
        for j, line in enumerate(lines):
            yield Text(
                name=FONT,
                size=64,
                text=line,
                position=V2(1350, 250 + 50 * j),
                anchor=V2(0, 0),
            )


def render_turn_button(state):
    yield Image(
        name="turn-button",
        position=V2(1820, 980),
        on_hover=('focus', 'turn-button'),
        on_click=('finish turn', None),
        animations=pmap({
            'scale': Animation(
                name='turn-button',
                start_value=1.0 if state.focus and state.focus == 'turn-button' else 1.2,
                end_value=1.2 if state.focus and state.focus == 'turn-button' else 1.0,
                duration=0.25,
            )
        })
    )

    yield Text(
        name=FONT,
        size=64 if state.focus and state.focus == 'turn-button' else 128,
        text='End Turn' if state.focus and state.focus == 'turn-button' else str(state.player.energy),
        position=V2(1820, 980),
        animations=pmap({
            'scale': Animation(
                name='turn-button',
                start_value=1.0 if state.focus and state.focus == 'turn-button' else 1.2,
                end_value=1.2 if state.focus and state.focus == 'turn-button' else 1.0,
                duration=0.25,
            )
        })
    )

    if state.focus and state.focus == 'turn-button':
        yield Image(
            name="smoke-background",
            anchor=V2(1, 0),
            position=V2(1920, 100),
        )
        lines = [
            'Ends the turn, converting',
            'unused energy into a growth',
            'tick for each unused energy',
        ]
        for j, line in enumerate(lines):
            yield Text(
                name=FONT,
                size=64,
                text=line,
                position=V2(1350, 250 + 50 * j),
                anchor=V2(0, 0),
            )


def render_overlay(state):
    for i, card in enumerate(state.player.deck.hand):
        if state.focus and state.focus == card:
            yield from card.render(
                name=f'overlay-{card.number}',
                start_scale=0.6,
                end_scale=1.0,
                start_position=V2(200 + i * 180, 1080),
                end_position=V2(200 + i * 180, 780),
                on_click=('use card', card),
            )

    end_point = V2(100, 800)
    start_point = V2(900, 550)
    number_of_items = len(state.player.creations.values())
    for i, (card_number, card) in enumerate(sorted(state.player.creations.items())):
        position = interpolate(start_point, end_point, (i + 1) / (number_of_items + 1))
        if state.focus and state.focus == card:
            yield from card.render(start_position=position, end_position=position)
            yield Image(
                name="smoke-background",
                anchor=V2(1, 0),
                position=V2(1920, 100),
            )
            lines = []
            lines.append(f'Growth: {card.growth:.03f}')
            growth_rate = card.growth_rate * state.growth_rate(state.player.faction) * state.player.growth_rate
            lines.append(f'Growth Rate: {growth_rate:.03f}')
            if hasattr(card, 'armor') and card.armor > 0.0:
                lines.append(f'Armor: {card.armor:.03f}')
            if hasattr(card, 'dot') and card.dot[0] > 0.0:
                lines.append(f'Taking {(1 - card.dot[0])*100:.0f}% damage/turn')
                lines.append(f'    for the next {card.dot[1]} turns')
            for j, line in enumerate(lines):
                yield Text(
                    name=FONT,
                    size=64,
                    text=line,
                    position=V2(1350, 250 + 50 * j),
                    anchor=V2(0, 0),
                )

    end_point = V2(1920 - 100, 800)
    start_point = V2(1920 - 900, 550)
    number_of_items = len(state.enemy.creations.values())
    for i, (card_number, card) in enumerate(sorted(state.enemy.creations.items())):
        position = interpolate(start_point, end_point, (i + 1) / (number_of_items + 1))
        if state.focus and state.focus == card:
            yield from card.render(start_position=position, end_position=position)
            yield Image(
                name="smoke-background",
                anchor=V2(1, 0),
                position=V2(1920, 100),
            )
            lines = []
            lines.append(f'Growth: {card.growth:.03f}')
            growth_rate = card.growth_rate * state.growth_rate(state.enemy.faction) * state.enemy.growth_rate
            lines.append(f'Growth Rate: {growth_rate:.03f}')
            if hasattr(card, 'armor') and card.armor > 0.0:
                lines.append(f'Armor: {card.armor:.03f}')
            if hasattr(card, 'dot') and card.dot[0] > 0.0:
                lines.append(f'Taking {card.dot[0]} damage over')
                lines.append(f'    the next {card.dot[1]} turns')
            for j, line in enumerate(lines):
                yield Text(
                    name=FONT,
                    size=64,
                    text=line,
                    position=V2(1350, 250 + 50 * j),
                    anchor=V2(0, 0),
                )


def stats_for_level(level):
    if level == 1:
        return 3, 2, 1.05
    elif level == 2:
        return 3, 2, 1.10
    elif level == 3:
        return 3, 2, 1.15
    elif level == 4:
        return 4, 3, 1.15
    elif level == 5:
        return 4, 3, 1.20
    elif level == 6:
        return 4, 3, 1.25
    elif level == 7:
        return 5, 4, 1.25
    elif level == 8:
        return 5, 4, 1.30
    elif level == 9:
        return 5, 4, 1.35
    else:
        additional = level - 9
        return 5 + additional, 4 + additional, 1.35 + 0.05 * additional


def make_new(mixer, level):
    energy, size, growth = stats_for_level(level)
    mixer.play_music(random.choice(['battle1', 'battle2']), 0.15, fade_duration=10.0)
    return State(
        enemy_level=level,
        player=Player(
            deck=Deck.load('player-plant-deck.json').shuffle(),
            faction='plant',
            max_energy=stats.max_energy(),
            hand_size=stats.hand_size(),
            growth_rate=stats.growth_rate()
        ),
        enemy=Player(
            deck=Deck.load('fungi-deck.json').shuffle(),
            faction='fungi',
            max_energy=energy,
            hand_size=size,
            growth_rate=growth,
        ),
    )
