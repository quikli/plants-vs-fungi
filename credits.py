from pyrsistent import PClass, field, pmap

import deck
import menu
from cards import Card

from renderables import Image, Text, Animation
from geometry import V2


FONT = "to-the-point-regular.ttf"

with open('credits.txt', 'r') as f:
    lines = f.read().split('\n')


class State(PClass):
    pass


def update(mixer, event, state):
    event_name, event_value = event
    if event_name == 'return':
        return menu.State()
    else:
        return state


def render(state):
    yield Image(name="generic_background", anchor=V2(0, 0), on_click=('return', None))
    for i, line in enumerate(lines):
        if line.strip() != "":
            yield Text(
                name=FONT,
                size=48,
                text=line,
                position=V2(960, 100 + 48 * i),
            )


