PGC Fantasy RPG Music Pack

Courtesdy of JP Soundworks
Licensed under CC BY 4.0
YouTube: https://www.youtube.com/c/JPSoundworks/
Platonic Game Studio: https://store.steampowered.com/developer/platonicgamestudio


To The Point Font

Courtesy of SavanasDesign
Licensed under the SIL Open Font License
Link: https://www.1001fonts.com/to-the-point-font.html


