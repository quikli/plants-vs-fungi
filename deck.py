import random
import json

from pyrsistent import freeze, thaw, PClass, field, pvector
from cards import Card


class Deck(PClass):
    collection = field()
    hand = field(mandatory=True, initial=pvector([]))
    deck = field(mandatory=True, initial=pvector([]))
    grave = field(mandatory=True, initial=pvector([]))

    @classmethod
    def load(cls, path):
        with open(path, 'r') as f:
            collection = json.load(f)
        return cls(collection=freeze(collection))

    def save(self, path):
        with open(path, 'w') as f:
            json.dump(thaw(self.collection), f)

    def add_to_collection(self, card):
        c = self.collection.get(card.name).evolver()
        c['in_deck'] += 1
        c['owned'] += 1
        return self.set(collection=self.collection.set(card.name, c.persistent()))

    def shuffle(self):
        deck = []
        for name, quantities in self.collection.items():
            for _ in range(quantities['in_deck']):
                deck.append(Card.spawn(name))
        random.shuffle(deck)
        return self.set(
            hand=pvector(),
            deck=pvector(deck),
            grave=pvector([]),
        )

    def draw(self, quantity):
        return self.set(
            hand=self.hand + self.deck[:quantity],
            deck=self.deck[quantity:],
        )

    def move_to_grave(self, card):
        hand_index = self.index(card)
        return self.set(
            hand=self.hand.delete(hand_index),
            grave=self.grave.append(self.hand[hand_index]),
        )

    def index(self, card):
        return list(c.number for c in self.hand).index(card.number)

    def remove_from_hand(self, card):
        return self.set(hand=self.hand.delete(self.index(card)))

    def remove_card(self, card_name):
        c = self.collection.get(card_name).evolver()
        if c['in_deck'] > 0:
            c['in_deck'] -= 1
        return self.set(collection=self.collection.set(card_name, c.persistent()))

    def add_card(self, card_name):
        c = self.collection.get(card_name).evolver()
        if c['in_deck'] < c['owned'] :
            c['in_deck'] += 1
        return self.set(collection=self.collection.set(card_name, c.persistent()))


def reset():
    import os
    os.system('cp player-plant-deck-default.json player-plant-deck.json')

