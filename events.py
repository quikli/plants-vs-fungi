import ctypes
import enum
import sdl2

import pyrsistent

from vector import V2
from clock import Clock


class Button(enum.Flag):
    NONE = 0b00000
    LEFT = 0b00001
    MIDDLE = 0b00010
    RIGHT = 0b00100
    EXTRA1 = 0b01000
    EXTRA2 = 0b10000
    ALL = 0b11111

    @classmethod
    def from_number(cls, number):
        if number == 1:
            return cls.LEFT
        elif number == 2:
            return cls.MIDDLE
        elif number == 3:
            return cls.RIGHT
        elif number == 4:
            return cls.EXTRA1
        elif number == 5:
            return cls.EXTRA2
        else:
            return cls.NONE


class Event(pyrsistent.PClass):
    name = pyrsistent.field(type=str, mandatory=True)


class QuitEvent(Event):
    pass


class UpdateEvent(Event):
    time_delta = pyrsistent.field(type=float, mandatory=True)
    current_time = pyrsistent.field(type=float, mandatory=True)


class RenderEvent(Event):
    current_time = pyrsistent.field(type=float, mandatory=True)


class MouseMove(Event):
    button = pyrsistent.field(type=Button, mandatory=True)
    position = pyrsistent.field(type=V2, mandatory=True)
    velocity = pyrsistent.field(type=V2, mandatory=True)


class MouseButtonEvent(Event):
    button = pyrsistent.field(type=Button, mandatory=True)
    position = pyrsistent.field(type=V2, mandatory=True)


class HoverEvent(Event):
    key = pyrsistent.field(mandatory=True)


class ClickEvent(Event):
    key = pyrsistent.field(mandatory=True)


class RendererResetEvent(Event):
    pass


def monitor_events(frame_rate=60.0):

    clock = Clock(1.0 / frame_rate)
    event = sdl2.SDL_Event()
    current_time = 0.0

    while True:

        while sdl2.SDL_PollEvent(ctypes.byref(event)):
            if event.type == sdl2.SDL_MOUSEMOTION:
                yield MouseMove(
                    name="mousemove",
                    button=Button(event.motion.state),
                    position=V2(event.motion.x, event.motion.y),
                    velocity=V2(event.motion.xrel, event.motion.yrel),
                )
            elif event.type == sdl2.SDL_MOUSEBUTTONUP:
                yield MouseButtonEvent(
                    name="mouseup",
                    button=Button.from_number(event.button.state),
                    position=V2(event.button.x, event.button.y),
                )
            elif event.type == sdl2.SDL_MOUSEBUTTONDOWN:
                yield MouseButtonEvent(
                    name="mousedown",
                    button=Button.from_number(event.button.state),
                    position=V2(event.button.x, event.button.y),
                )
            elif event.type == sdl2.SDL_QUIT:
                yield QuitEvent(name="quit")
                return
            elif event.type == sdl2.SDL_RENDER_TARGETS_RESET:
                yield RendererResetEvent(name='reset')
            else:
                pass

        time_delta = clock.time_since("update")
        current_time += time_delta

        yield UpdateEvent(name="update", time_delta=time_delta, current_time=current_time)
        yield RenderEvent(name="render", current_time=current_time)

        clock.end_frame()
