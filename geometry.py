from pyrsistent import immutable, PClass, field
from math import radians, degrees, pi, cos, sin, atan2, sqrt


class V2(immutable(["x", "y"])):
    @classmethod
    def from_degrees(cls, angle, length=1.0):
        angle = radians(angle)
        x = cos(angle) * length
        y = sin(angle) * length
        return cls(x, y)

    @classmethod
    def from_radians(cls, angle, length=1.0):
        x = cos(angle) * length
        y = sin(angle) * length
        return cls(x, y)

    def __str__(self):
        return f"V2({self.x:.3f}, {self.y:.3f})"

    def __repr__(self):
        return f"V2({self.x:.3f}, {self.y:.3f})"

    def __add__(self, other):
        return self.__class__(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return self.__class__(self.x - other.x, self.y - other.y)

    def __neg__(self):
        return self.__class__(-self.x, -self.y)

    def __mul__(self, factor):
        return self.__class__(self.x * factor, self.y * factor)

    def __truediv__(self, factor):
        return self.__class__(self.x / factor, self.y / factor)

    @property
    def ints(self):
        return int(self.x), int(self.y)

    @property
    def floats(self):
        return float(self.x), float(self.y)

    @property
    def length(self):
        return sqrt(self.x ** 2 + self.y ** 2)

    @property
    def normalize(self):
        return self / self.length

    @property
    def x_vector(self):
        return self.set(y=0.0)

    @property
    def y_vector(self):
        return self.set(x=0.0)

    @property
    def mirror(self):
        return -self

    @property
    def mirror_x(self):
        return self.set(y=-self.y)

    @property
    def mirror_y(self):
        return self.set(x=-self.x)

    @property
    def radians(self):
        return atan2(self.y, self.x)

    @property
    def degrees(self):
        return degrees(self.radians)

    def dot_product(self, other):
        return self.x * other.x + self.y * other.y

    def cross_product(self, other):
        return self.x * other.y - self.y * other.x

    def rotate_degrees(self, degrees):
        return self.from_degrees(degrees + self.degrees, self.length)

    def rotate_radians(self, radians):
        return self.from_radians(radians + self.radians, self.length)

    def vector_multiply(self, other):
        return self.set(x=self.x * other.x, y=self.y * other.y)


class Rect(PClass):
    position = field(type=V2, mandatory=True, initial=V2(0, 0))
    size = field(type=V2, mandatory=True)

    @property
    def x(self):
        return self.position.x

    @property
    def y(self):
        return self.position.y

    @property
    def w(self):
        return self.size.x

    @property
    def h(self):
        return self.size.y

    def contains(self, point):
        if not (self.position.x <= point.x <= self.position.x + self.size.x):
            return False
        elif not (self.position.y <= point.y <= self.position.y + self.size.y):
            return False
        else:
            return True

    def aspect_scale(self, other):
        size1 = self.size
        size2 = other.size
        source_aspect = size1.x / size1.y
        target_aspect = size2.x / size2.y
        scale = size2.x / size1.x if source_aspect > target_aspect else size2.y / size1.y
        size = V2(size1.x * scale, size1.y * scale)
        position = V2((size2.x - size.x) / 2, (size2.y - size.y) / 2)
        return Rect(position=other.position + position, size=size), scale
