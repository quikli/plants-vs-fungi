from pyrsistent import PClass, field, pmap

import deck
import menu
from cards import Card

from renderables import Image, Text, Animation
from geometry import V2


FONT = "to-the-point-regular.ttf"


class State(PClass):
    deck = field(type=deck.Deck, mandatory=True)
    focus = field(initial=None)


def update(mixer, event, state):
    event_name, event_value = event
    if event_name == 'focus':
        if event_value and event_value != state.focus and not event_value in state.deck.collection:
            mixer.play_sfx('click', 1.0)
        if event_value and event_value != state.focus and event_value in state.deck.collection:
            mixer.play_sfx('deal2', 1.0)
        return state.set(focus=event_value)
    elif event_name == 'remove card':
        mixer.play_sfx('playcard', 1.0)
        deck = state.deck.remove_card(event_value)
        deck.save('player-plant-deck.json')
        return state.set(deck=deck)
    elif event_name == 'add card':
        mixer.play_sfx('deal1', 1.0)
        deck = state.deck.add_card(event_value)
        deck.save('player-plant-deck.json')
        return state.set(deck=deck)
    elif event_name == 'return':
        mixer.play_sfx('playcard', 1.0)
        return menu.State()
    else:
        return state


def render(state):
    yield Image(name="generic_background", anchor=V2(0, 0), on_hover=('focus', None))

    for i, card in enumerate(state.deck.collection):
        x = i % 3
        y = i // 3
        yield from Card.spawn(card).render(
            start_position=V2(125 + x * 475, 200 + y * 350),
            end_position=V2(125 + x * 475, 200 + y * 350),
            start_scale=0.5,
            end_scale=0.5,
            on_hover=('focus', card),
        )

        stats = state.deck.collection.get(card)
        owned = stats['owned']
        in_deck = stats['in_deck']

        yield Text(
            name=FONT,
            size=48,
            text=f'Owned: {owned}',
            position=V2(325 + x * 475, 200 + y * 350 - 125),
        )

        yield Text(
            name=FONT,
            size=48,
            text=f'In Deck: {in_deck}',
            position=V2(325 + x * 475, 200 + y * 350 - 75),
        )

        if owned > in_deck:
            end_scale = 64 if state.focus and state.focus == f'add {card}' else 48
            start_scale = 48 if state.focus and state.focus == f'add {card}' else 64
            yield Text(
                name=FONT,
                size=48,
                text='Add Card',
                position=V2(325 + x * 475, 200 + y * 350 - 25),
                on_hover=('focus', f'add {card}'),
                on_click=('add card', card),
                animations=pmap({
                    'size': Animation(
                        name=f'add {card}',
                        start_value=start_scale,
                        end_value=end_scale,
                        duration=0.2,
                    ),
                })
            )

        if in_deck > 0:
            end_scale = 64 if state.focus and state.focus == f'remove {card}' else 48
            start_scale = 48 if state.focus and state.focus == f'remove {card}' else 64
            yield Text(
                name=FONT,
                size=48,
                text='Remove Card',
                position=V2(325 + x * 475, 200 + y * 350 + 25),
                on_hover=('focus', f'remove {card}'),
                on_click=('remove card', card),
                animations=pmap({
                    'size': Animation(
                        name=f'remove {card}',
                        start_value=start_scale,
                        end_value=end_scale,
                        duration=0.2,
                    ),
                })
            )

        if state.focus and state.focus == card:
            yield from Card.spawn(card).render(
                start_position=V2(1600, 2000),
                end_position=V2(1600, 1080/2),
                start_scale=0.0,
                end_scale=1.0,
                on_hover=('focus', card),
            )


    end_scale = 128 if state.focus and state.focus == 'return' else 96
    start_scale = 96 if state.focus and state.focus == 'return' else 128
    yield Text(
        name=FONT,
        size=96,
        text='Return to Menu',
        position=V2(1600, 100),
        on_hover=('focus', 'return'),
        on_click=('return', 'return'),
        animations=pmap({
            'size': Animation(
                name='return',
                start_value=start_scale,
                end_value=end_scale,
                duration=0.2,
            ),
        })
    )


