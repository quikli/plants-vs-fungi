from pyrsistent import PClass, field, pmap
from renderables import Image, Text, Animation
from geometry import V2


FONT = "to-the-point-regular.ttf"


class State(PClass):
    selection = field(type=str, mandatory=True, initial='')
    complete = field(type=bool, mandatory=True, initial=False)


def update(mixer, event, state):
    event_name, event_value = event
    if event_name == 'change':
        if event_value != state.selection:
            mixer.play_sfx('click', 1.0)
            return state.set(selection=event_value)
        else:
            return state
    elif event_name == 'confirm':
        mixer.play_sfx('playcard', 1.0)
        return state.set(selection=event_value, complete=True)
    else:
        return state


def render(state):
    yield Image(name="menu", anchor=V2(0, 0), on_hover=('change', ''))

    for i, line in enumerate(['Play Endless', 'Manage Deck', 'Credits', 'Quit']):
        end_size = 128 if state.selection == line else 96
        start_size = 96 if state.selection == line else 128
        yield Text(
            name=FONT,
            size=128,
            text=line,
            position=V2(1920 / 2, 590 + i * 100),
            on_hover=('change', line),
            on_click=('confirm', line),
            animations=pmap({
                'size': Animation(
                    start_value=start_size,
                    end_value=end_size,
                    duration=0.5,
                ),
            })
        )

