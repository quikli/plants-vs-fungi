import sdl2.sdlmixer as mix
import sdl2 as sdl

from pyrsistent import PClass, field
assert mix.Mix_Init(mix.MIX_INIT_OGG) == mix.MIX_INIT_OGG
assert mix.Mix_OpenAudio(mix.MIX_DEFAULT_FREQUENCY * 2, mix.MIX_DEFAULT_FORMAT, mix.MIX_DEFAULT_CHANNELS, 1024) >= 0

from functools import cache, partial
from utils import interpolate, composition


class Fade(PClass):
    start_volume = field(type=float, mandatory=True)
    end_volume = field(type=float, mandatory=True)
    action_on_end = field(mandatory=True)
    start_time = field(mandatory=True, initial=None)
    current_time = field(mandatory=True, initial=None)
    fade_duration = field(type=float, mandatory=True)

    def advance(self, current_time):
        if self.start_time is None:
            return self.set(start_time=current_time, current_time=current_time)
        else:
            return self.set(current_time=current_time)

    @property
    def percent_complete(self):
        comp = composition(self.start_time, self.start_time + self.fade_duration, self.current_time)
        return 1.0 if comp > 1 else comp

    @property
    def is_complete(self):
        return self.percent_complete == 1.0

    @property
    def current_volume(self):
        return interpolate(self.start_volume, self.end_volume, self.percent_complete)


class Mixer:
    def __init__(self):
        self.music_playing = ""
        self.fades = []
        self.last_target_volume = 0.0

    def advance(self, current_time):
        if len(self.fades) > 0:
            fade = self.fades[0].advance(current_time)
            current_volume = fade.current_volume
            mix.Mix_VolumeMusic(int(current_volume * 255))
            if fade.is_complete:
                fade.action_on_end(self)
                self.fades = self.fades[1:]
            else:
                self.fades[0] = fade

    @cache
    def _load_music(self, name):
        return mix.Mix_LoadMUS(f"music/{name}.ogg".encode('utf-8'))

    @cache
    def _load_sfx(self, name):
        return mix.Mix_LoadWAV_RW(
            sdl.SDL_RWFromFile(f"sfx/{name}.wav".encode('utf-8'),
                'rb'.encode('utf-8')), 1
        )

    def play_music(self, music, volume, fade_duration=2.0):
        if self.music_playing != "" and self.music_playing != music:
            self.fades.append(Fade(
                start_volume=self.last_target_volume,
                end_volume=0.0,
                action_on_end=partial(lambda m: m.switch_track(music)),
                fade_duration=1.0,
            ))
            self.fades.append(Fade(
                start_volume=0.0,
                end_volume=volume,
                action_on_end=partial(lambda m: None),
                fade_duration=fade_duration,
            ))
        elif self.music_playing != music:
            self.switch_track(music)
            self.fades.append(Fade(
                start_volume=self.last_target_volume,
                end_volume=volume,
                action_on_end = partial(lambda m: None),
                fade_duration=fade_duration,
            ))
        self.last_target_volume = volume

    def switch_track(self, music):
        if self.music_playing != "":
            mix.Mix_HaltMusic()
        mix.Mix_PlayMusic(self._load_music(music), -1)
        self.music_playing = music

    def play_sfx(self, sfx, volume):
        sfx = self._load_sfx(sfx)
        mix.Mix_VolumeChunk(sfx, int(volume * 255))
        mix.Mix_PlayChannelTimed(-1, sfx, 0, -1)

