from pyrsistent import PClass, field, pmap_field

from vector import V2, Rect
from utils import composition, interpolate


ANIMATIONS = {}
TOUCHED_ANIMATIONS = set()


class Animation(PClass):
    name = field(mandatory=True, initial='')
    start_value = field(mandatory=True)
    end_value = field(mandatory=True)
    duration = field(type=float, mandatory=True, initial=1.0)

    def value(self, time):
        global ANIMATIONS
        global TOUCHED_ANIMATIONS
        TOUCHED_ANIMATIONS.add(self)
        if self in ANIMATIONS:
            start_time = ANIMATIONS[self]
            if time - start_time > self.duration:
                return self.end_value
            else:
                return interpolate(
                    self.start_value,
                    self.end_value,
                    composition(
                        start_time,
                        start_time + self.duration,
                        time
                    )
                )
        else:
            ANIMATIONS[self] = time
            return self.start_value


class Renderable(PClass):
    position = field(type=V2, mandatory=True, initial=V2(0, 0))
    scale = field(type=float, mandatory=True, initial=1.0)
    anchor = field(type=V2, mandatory=True, initial=V2(0.5, 0.5))
    animations = pmap_field(str, Animation)

    def _position(self, time):
        if 'position' in self.animations:
            return self.animations['position'].value(time)
        else:
            return self.position

    def _scale(self, time):
        if 'scale' in self.animations:
            return self.animations['scale'].value(time)
        else:
            return self.scale

    def _anchor(self, time):
        if 'anchor' in self.animations:
            return self.animations['anchor'].value(time)
        else:
            return self.anchor

    def render(self, renderer, time):
        pass

    def calc_rect(self, size, scale, time):
        size = size * scale
        anchor = self._anchor(time).vector_multiply(size)
        position = self._position(time) - anchor
        return Rect(position=position, size=size)


class FilledRect(Renderable):
    size = field(type=V2, mandatory=True, initial=V2(100, 100))
    color = field(type=tuple, mandatory=True, initial=(255, 0, 255, 255))

    def _size(self, time):
        if 'size' in self.animations:
            return self.animations['size'].value(time)
        else:
            return self.size

    def render(self, renderer, time):
        rect = self.calc_rect(self._size(time), self._scale(time), time)
        renderer.fill_rect(rect, self.color)


class Image(Renderable):
    name = field(type=str, mandatory=True)
    on_hover = field(mandatory=True, initial=None)
    on_click = field(mandatory=True, initial=None)
    interaction_scale = field(type=float, mandatory=True, initial=1.0)
    is_composite = field(type=bool, mandatory=True, initial=False)

    def get_image(self, renderer, time):
        if self.is_composite:
            return renderer.composites[self.name]
        else:
            return renderer.load_image(self.name)

    def render(self, renderer, time):
        image, size = self.get_image(renderer, time)
        rect = self.calc_rect(size, self._scale(time), time)
        int_rect = self.calc_rect(size, self._scale(time) * self.interaction_scale, time)
        renderer.draw_image(image, rect)
        if self.on_hover:
            renderer.on_hover(int_rect, self.on_hover)
        if self.on_click:
            renderer.on_click(int_rect, self.on_click)


class Text(Image):
    size = field(type=int, mandatory=True, initial=48)
    text = field(type=str, mandatory=True)
    wrap = field(type=int, mandatory=True, initial=0)
    color = field(type=tuple, mandatory=True, initial=(255, 255, 255, 255))

    def _size(self, time):
        if 'size' in self.animations:
            return self.animations['size'].value(time)
        else:
            return self.size

    def get_image(self, renderer, time):
        return renderer.rasterize_text(self.name, int(self._size(time)), self.text, self.wrap, self.color)

