import ctypes
import enum
from functools import cache

DEBUG = False

from sdl2 import (
    SDL_Color,
    SDL_CreateWindow,
    SDL_CreateRenderer,
    SDL_CreateTexture,
    SDL_CreateTextureFromSurface,
    SDL_Rect,
    SDL_FreeSurface,
    SDL_GetError,
    SDL_GetRendererOutputSize,
    SDL_RenderClear,
    SDL_RenderCopy,
    SDL_RenderFillRect,
    SDL_RenderDrawRect,
    SDL_RenderPresent,
    SDL_SetHint,
    SDL_SetRenderDrawColor,
    SDL_SetRenderTarget,
    SDL_SetTextureBlendMode,
)

from sdl2 import (
    SDL_BLENDMODE_BLEND,
    SDL_PIXELFORMAT_RGBA8888,
    SDL_RENDERER_ACCELERATED,
    SDL_RENDERER_PRESENTVSYNC,
    SDL_TEXTUREACCESS_TARGET,
    SDL_WINDOW_RESIZABLE,
    SDL_WINDOW_SHOWN,
    SDL_WINDOW_FULLSCREEN_DESKTOP,
    SDL_WINDOWPOS_UNDEFINED,
)

from sdl2.sdlimage import (
    IMG_Load,
)

import sdl2.sdlttf as ttf

from geometry import V2, Rect
import renderables


class Renderer:
    def __init__(self, config):
        window_title = config['NAME']
        vsync = SDL_RENDERER_PRESENTVSYNC if config['VSYNC'] else 0
        resizable = SDL_WINDOW_RESIZABLE if config['RESIZABLE'] else 0
        fullscreen = SDL_WINDOW_FULLSCREEN_DESKTOP if config['FULLSCREEN'] else 0
        self.window = SDL_CreateWindow(
            window_title.encode("utf-8"),
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            int(config['RESOLUTION'].x),
            int(config['RESOLUTION'].y),
            resizable | fullscreen | SDL_WINDOW_SHOWN,
        )
        self.renderer = SDL_CreateRenderer(
            self.window,
            -1,
            SDL_RENDERER_ACCELERATED | vsync,
        )
        self.buffer = SDL_CreateTexture(
            self.renderer,
            SDL_PIXELFORMAT_RGBA8888,
            SDL_TEXTUREACCESS_TARGET,
            int(config['RESOLUTION'].x),
            int(config['RESOLUTION'].y),
        )
        self.resolution = config['RESOLUTION']
        self.composites = {}
        self.clickboxes = []
        self.hoverboxes = []

    @cache
    def _load_font(self, path, size):
        font = ttf.TTF_OpenFont(path.encode("utf-8"), size)
        return font

    @cache
    def rasterize_text(self, path, size, text, wrap_length, color):
        font = self._load_font(path, size)
        color = SDL_Color(*color)
        if wrap_length > 0:
            surface = ttf.TTF_RenderText_Blended_Wrapped(
                font, text.encode("utf-8"), color, wrap_length
            )
        else:
            surface = ttf.TTF_RenderText_Blended(font, text.encode("utf-8"), color)
        texture = SDL_CreateTextureFromSurface(self.renderer, surface)
        result = texture, V2(surface.contents.w, surface.contents.h)
        SDL_FreeSurface(surface)
        return result

    @cache
    def _load_image(self, path):
        surface = IMG_Load(f"images/{path}.png".encode("utf-8"))
        return surface

    @cache
    def load_image(self, path):
        surface = self._load_image(path)
        texture = SDL_CreateTextureFromSurface(self.renderer, surface)
        return texture, V2(surface.contents.w, surface.contents.h)

    def create_named_composite_image(self, name, size, renderables):
        if name in self.composites:
            return self.composites[name]
        result_texture = SDL_CreateTexture(
            self.renderer,
            SDL_PIXELFORMAT_RGBA8888,
            SDL_TEXTUREACCESS_TARGET,
            int(size.x),
            int(size.y),
        )
        SDL_SetTextureBlendMode(result_texture, SDL_BLENDMODE_BLEND)
        SDL_SetRenderTarget(self.renderer, result_texture)
        for renderable in renderables:
            renderable.render(self, 0.0)
        self.composites[name] = result_texture, size
        SDL_SetRenderTarget(self.renderer, self.buffer)

    def fill_rect(self, rect, color):
        dest_rect = self._rect_to_sdl(rect)
        SDL_SetRenderDrawColor(self.renderer, *color)
        SDL_RenderFillRect(self.renderer, dest_rect)
        SDL_SetRenderDrawColor(self.renderer, 0, 0, 0, 255)

    def _rect_to_sdl(self, rect):
        return SDL_Rect(int(rect.x), int(rect.y), int(rect.w), int(rect.h))

    def draw_rect(self, rect):
        dest_rect = self._rect_to_sdl(rect)
        SDL_SetRenderDrawColor(self.renderer, 255, 0, 255, 255)
        SDL_RenderDrawRect(self.renderer, dest_rect)
        SDL_SetRenderDrawColor(self.renderer, 0, 0, 0, 255)

    def draw_image(self, image, rect):
        dest_rect = self._rect_to_sdl(rect)
        SDL_RenderCopy(self.renderer, image, None, ctypes.byref(dest_rect))

    def on_click(self, rect, interaction):
        #self.draw_rect(rect)
        self.clickboxes.append((interaction, rect))

    def on_hover(self, rect, interaction):
        #self.draw_rect(rect)
        self.hoverboxes.append((interaction, rect))

    def clear(self):
        self.clickboxes = []
        self.hoverboxes = []
        SDL_SetRenderTarget(self.renderer, self.buffer)
        SDL_RenderClear(self.renderer)

    @property
    def aspect_scale_metrics(self):
        tw = ctypes.c_int(0)
        th = ctypes.c_int(0)
        SDL_GetRendererOutputSize(self.renderer, ctypes.byref(tw), ctypes.byref(th))
        buffer = Rect(position=V2(0, 0), size=self.resolution)
        target = Rect(position=V2(0, 0), size=V2(tw.value, th.value))
        return buffer.aspect_scale(target)

    def display(self):
        SDL_SetRenderTarget(self.renderer, None)
        SDL_RenderClear(self.renderer)
        rect, scale = self.aspect_scale_metrics
        target = self._rect_to_sdl(rect)
        SDL_RenderCopy(self.renderer, self.buffer, None, ctypes.byref(target))
        SDL_RenderPresent(self.renderer)
        to_delete = [a for a in renderables.ANIMATIONS if a not in renderables.TOUCHED_ANIMATIONS]
        for animation in to_delete:
            del renderables.ANIMATIONS[animation]
        renderables.TOUCHED_ANIMATIONS = set()


