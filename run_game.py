import sys
import time

import sdl2 as sdl
import sdl2.sdlimage as img
import sdl2.sdlttf as ttf

from pyrsistent import PClass, field, pmap

import cards
import credits
import menu
import stats
import combat
import deck
import managedeck

from events import monitor_events
from mixer import Mixer
from renderer import Renderer
from renderables import Image, Text
from geometry import V2

sdl.SDL_Init(sdl.SDL_INIT_EVERYTHING)
img.IMG_Init(img.IMG_INIT_PNG)
ttf.TTF_Init()


CONFIG = {
    'NAME': "Plants vs. Fungi",
    'VSYNC': True,
    'FULLSCREEN': False,
    'RESIZABLE': True,
    'RESOLUTION': V2(1920, 1080),
    'FRAME_RATE': 60.0,
}


def main(config):
    renderer = Renderer(config)
    mixer = Mixer()
    max_frame_rate = config['FRAME_RATE']
    mode = None
    state = None

    def module():
        if mode == 'menu':
            return menu
        if mode == 'combat':
            return combat
        if mode == 'manage':
            return managedeck
        if mode == 'credits':
            return credits

    def world_transform(renderer, event):
        if hasattr(event, "position") or hasattr(event, "position"):
            rect, scale = renderer.aspect_scale_metrics
            if hasattr(event, "position"):
                event = event.set(position=(event.position - rect.position) / scale)
            if hasattr(event, "velocity"):
                event = event.set(velocity=(event.velocity - rect.position) / scale)
        return event

    def update(mixer, action):
        nonlocal mode, state
        new_state = module().update(mixer, action, state)
        if mode == 'menu':
            if new_state.complete:
                if new_state.selection == 'Play Endless':
                    to_combat(mixer)
                elif new_state.selection == 'Manage Deck':
                    to_management(mixer)
                elif new_state.selection == 'Credits':
                    to_credits(mixer)
                elif new_state.selection == 'Quit':
                    sys.exit()
                else:
                    state = new_state
            else:
                state = new_state
        elif isinstance(new_state, menu.State):
            to_menu(mixer)
        else:
            state = new_state

    def to_combat(mixer):
        nonlocal mode, state
        mode = 'combat'
        state = combat.make_new(mixer, 1)

    def to_menu(mixer):
        nonlocal mode, state
        mode = 'menu'
        mixer.play_music('menu', 0.3)
        state = menu.State()

    def to_management(mixer):
        nonlocal mode, state
        mode = 'manage'
        state = managedeck.State(deck=deck.Deck.load('player-plant-deck.json'))

    def to_credits(mixer):
        nonlocal mode, state
        mode = 'credits'
        state = credits.State()

    def preload():
        for name, card in cards.CARDS.items():
            renderer.create_named_composite_image(
                name, V2(400, 600), [
                    Image(name=f"card-background-{card['type']}", position=V2(200, 300)),
                    Image(name=name, position=V2(200, 300)),
                    Image(name=f"{card['faction']}-frame", position=V2(200, 300)),
                ]
            )

    to_menu(mixer)

    preload()

    for event in monitor_events(max_frame_rate):
        event = world_transform(renderer, event)
        if event.name == "render":
            renderer.clear()
            for renderable in module().render(state):
                renderable.render(renderer, event.current_time)
            renderer.display()
        elif event.name == "mousemove":
            for action, rect in reversed(renderer.hoverboxes):
                if rect.contains(event.position):
                    update(mixer, action)
                    break
        elif event.name == "mousedown":
            for action, rect in reversed(renderer.clickboxes):
                if rect.contains(event.position):
                    update(mixer, action)
                    break
        elif event.name == "update":
            update(mixer, ('elapsed', event))
            mixer.advance(event.current_time)
        elif event.name == 'reset':
            renderer.load_image.cache_clear()
            renderer.composites = {}
            preload()


if __name__ == '__main__':
    #stats.reset()
    #deck.reset()
    main(CONFIG)

