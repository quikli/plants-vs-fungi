import json


def load():
    with open('player-stats.json', 'r') as f:
        stats = json.load(f)
        return stats


def save(stats):
    with open('player-stats.json', 'w') as f:
        json.dump(stats, f)


def reset():
    with open('player-stats-default.json', 'r') as f:
        stats = json.load(f)
    save(stats)


def add_reward(reward):
    stats = load()
    stats['rewards'].append(reward)
    stats['level'] = stats['level'] + 1
    save(stats)


def add_experience(exp):
    stats = load()
    stats['experience'] = stats['experience'] + exp
    save(stats)


def max_energy():
    stats = load()
    energy = 2
    for reward in stats['rewards']:
        if reward == 'Increased Energy per Turn (+1 Energy)':
            energy += 1
    return energy


def growth_rate():
    stats = load()
    growth_rate = 1.05
    for reward in stats['rewards']:
        if reward == 'Increased Growth Rate (+5%)':
            growth_rate += 0.05
    return growth_rate


def hand_size():
    stats = load()
    hand_size = 4
    for reward in stats['rewards']:
        if reward == 'Increased Hand Size (+1 Card)':
            hand_size += 1
    return hand_size


def get_level():
    stats = load()
    return stats['level']


def get_experience():
    stats = load()
    return stats['experience']


def experience_for(level):
    levels = [
        0,
        0,
        300,
        900,
        2100,
        4200,
        7560,
        12600,
        19800,
        29700,
        42900,
        60060,
    ]
    if level > 11:
        return level * 10000
    else:
        return levels[level]


