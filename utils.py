def clamp(v, l, u):
    if v < l:
        return l
    elif v > u:
        return u
    else:
        return v


def composition(min_value, max_value, actual_value):
    return (actual_value - min_value) / (max_value - min_value)


def interpolate(start_value, end_value, composition):
    return start_value + (end_value - start_value) * composition

